<?php

namespace App\Services;

use App\Entity\News;
use Doctrine\ORM\EntityManagerInterface;

class NewsService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * @param string $name
     * @param string $content
     */
    public function create(string $name, string $content)
    {
        $this->add(
            (new News)->setTitle($name)->setContent($content)
        );
    }

    /**
     * @param News $news
     */
    public function add(News $news)
    {
        $this->entityManager->persist($news);
        $this->entityManager->flush();
    }

    /**
     * @param News $news
     */
    public function delete(News $news)
    {
        $this->entityManager->remove($news);
        $this->entityManager->flush();
    }
}