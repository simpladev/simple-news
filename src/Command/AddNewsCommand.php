<?php

namespace App\Command;

use App\Entity\News;
use App\Services\NewsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Exception\RuntimeException;

class AddNewsCommand extends Command
{
    private $entityManager;

    /**
     * @var NewsService
     */
    private $service;

    /**
     * AddNewsCommand constructor.
     *
     * @param NewsService $service
     */
    public function __construct(NewsService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    protected function configure()
    {
        $this
            ->setName('news:add')
            ->setDescription('Add news');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $title   = $helper->ask($input, $output, new Question('Title: '));
        $content = $helper->ask($input, $output, new Question('Content: '));

        if (!$title || !$content) {
            $output->writeln('<info>All fields are required!</info>');
            throw new RuntimeException(sprintf('All fields are required!'));
        }

        $this->service->create($title, $content);

        $output->writeln('<info>Done!</info>');
    }
}
